---
title: "Feature selection (PCA)"
author: "Davide Garolini"
date: "4 Feb 2019"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{feature_selectionPCA}
  %\usepackage[utf8]{inputenc}
---



## Feature selection

Using the default R PCA algorithms (or the enhanced ones which are slow (PCAproj)) this function select the features with the biggest loading. It is also possible to plot the result in 2 components.


```r
library(mlbench); library(CampaRi)
data(PimaIndiansDiabetes)
# ?PimaIndiansDiabetes
# summary(PimaIndiansDiabetes)
# dim(PimaIndiansDiabetes)
slected_elements <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca') # automatically selected the first 2 components
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
## Returning the selected features...
```

```r
# slected_elements_robust <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', pca_method = 'robust') # projection pursuit 2 components

# the selected_elements* objects are data_frames with 2 columns (selected variables)
# If I want to select more variables:
slected_elements <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', n_princ_comp = 4) 
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 3 4 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
## Returning the selected features...
```

## Including Plots

You can also generate plots, for example:


```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-1.png)

```
## Returning the selected features...
```

```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, 
                cluster_vector = sample(c(1,2,3), size = nrow(PimaIndiansDiabetes), replace = T))
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-2.png)

```
## Returning the selected features...
```

```r
# let's use a better clustering definition:
clu_vector <- PimaIndiansDiabetes[,9]
clu_vector <- as.factor(clu_vector)
levels(clu_vector) <- c(2,1)
clu_vector <- as.numeric(clu_vector)


select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, cluster_vector = clu_vector)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-3.png)

```
## Returning the selected features...
```

```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, frameit = T, cluster_vector = clu_vector)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-4.png)

```
## Returning the selected features...
```

```r
plot1 <- select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', plotit = T, frameit = T, return_plot = T, cluster_vector = clu_vector)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
## Returning the plot object. No selected features will be returned with this mode active.
```

```r
# adding the legend?
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = T, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-5.png)

```
## Returning the selected features...
```

```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = F, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T, specific_palette = c("#b47b00", "#000000"))
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-6.png)

```
## Returning the selected features...
```

```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = F, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T, specific_palette = c("#b47b00", "#000000"))
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-7.png)

```
## Returning the selected features...
```

```r
select_features(PimaIndiansDiabetes[,1:8], feature_selection = 'pca', 
                plotit = T, frameit = T, points_size = 1.3, cluster_vector = clu_vector,
                plot_legend = T)
```

```
## Feature selection mode active. pca dimensionality reduction will be performed.
## PCA performed successfully.
## The most indipendent values selected are: 5 2 
## The details of the pca output will be oscurated but the selected values will be written in "selected_pca.out" ASCII file.
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-8.png)

```
## Returning the selected features...
```






