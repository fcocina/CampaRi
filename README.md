
[![pipeline status](https://gitlab.com/CaflischLab/CampaRi/badges/master/pipeline.svg)](https://gitlab.com/CaflischLab/CampaRi/commits/master)
[![coverage report](https://gitlab.com/CaflischLab/CampaRi/badges/master/coverage.svg)](https://codecov.io/gl/CaflischLab/CampaRi)
[![DOI](https://zenodo.org/badge/68593949.svg)](https://zenodo.org/badge/latestdoi/68593949)


------------------------------------------------------------------------
# CampaRi - An R wrapper for fast trajectory analysis

For any help with the package usage and installation please [chat with us](https://gitter.im/CampaRi_chat/Lobby).

The documentation of the package is available online on [CampaRi doc](https://caflischlab.gitlab.io/CampaRi)

CampaRi is a package for the analysis of time-series. Initially it was extracted from the original [campari](http://campari.sourceforge.net/documentation.html) software package, but it has been extended with other functionalities
like Markov state models analysis and network inference pre-processing (NetSAP). Please use the tutorials or the function documentation to explore all these possibilities.

------------------------------------------------------------------------
### Installation ###
The code is available on [this](https://gitlab.com/CaflischLab/CampaRi) GitLab repository and therefore it can be installed using "devtools".
The package is tested on Ubuntu 16.04. 

* Stable version: We will upload the package on CRAN as soon as possible.

* Development version: `devtools::install_git(url = "https://gitlab.com/CaflischLab/CampaRi")`

For further details on the installation procedure (which can be problematic for certain advanced functionality) we advise to read the installation tutorial.

------------------------------------------------------------------------
### Usage - tests ###

We are currently writing how-to-use guides with examples and datasets. The tutorial/vignettes are available internally in the package and on [this website](https://caflischlab.gitlab.io/CampaRi).


------------------------------------------------------------------------
### Extensive Manual ###

For more details, please refer to the main documentation of the original [CAMPARI documentation](http://campari.sourceforge.net/documentation.html).


------------------------------------------------------------------------
## Citation

Please cite the software and the SAPPHIRE algorithm paper:
* Davide Garolini, & Francesco Cocina. (2018, June 4). CampaRi: an R package for time series analysis (Version 0.9.1). Zenodo. http://doi.org/10.5281/zenodo.1260973
* Bloechliger, N., Vitalis, A. & Caflisch, A. A scalable algorithm to order and annotate continuous observations reveals the metastable states visited by dynamical systems. Comput. Phys. Commun. 184, 2446–2453 (2013).
