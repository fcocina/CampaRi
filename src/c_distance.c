#include <stdio.h>
#include <math.h>
#include <float.h>

#define ISNAN(x) (isnan(x)!=0)
#define R_FINITE(x)    R_finite(x)
#define ML_POSINF INFINITY
#define ML_NAN NAN
#define NA_REAL ML_NAN
#define both_FINITE(a,b) (R_FINITE(a) && R_FINITE(b))
#ifdef R_160_and_older
#define both_non_NA both_FINITE
#else
#define both_non_NA(a,b) (!ISNAN(a) && !ISNAN(b))
#endif

/*
function euclidean_d ( veci, vecj ) bind ( C, name='euclidean_d_c' )
  use, intrinsic :: iso_c_binding
  real ( c_double ) :: veci(*)
  real ( c_double ) :: vecj(*)
  real ( c_double ) :: euclidean_d
end function euclidean_d
*/
// #define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))
int R_finite(double x)
{
#ifdef HAVE_WORKING_ISFINITE
    return isfinite(x);
# else
    return (!isnan(x) & (x != INFINITY) & (x != -INFINITY));
#endif
}
//
// double R_pow_di(double x, int n)
// {
//     double pow = 1.0;
//
//     if (ISNAN(x)) return x;
//     if (n != 0) {
// 	     if (n < 0) {
//          n = -n; x = 1/x;
//        }
// 	     for(;;) {
// 	        if(n & 01) pow *= x;
// 	        if(n >>= 1) x *= x; else break;
// 	     }
//     }
//     return pow;
// }
static double unroll_dot(int N, double *A, double *B) {
    double dot1 = 0, dot2 = 0, dot3 = 0, dot4 = 0;

    for(int i = 0; i < N; i += 4) {
        dot1 += A[i] * B[i];
        dot2 += A[i + 1] * B[i + 1];
        dot3 += A[i + 2] * B[i + 2];
        dot4 += A[i + 3] * B[i + 3];
    }

    return dot1 + dot2 + dot3 + dot4;
}

double euclidean_d_c(double *veci, double *vecj, int ni) {
  double dev, dist;
  int j;
  // int ni = NELEMS(veci); // size_t can be potentially always bigger
  // size_t nj = NELEMS(vecj); // would get longer!
  dist = 0;
  // printf("%d |", ni);
  // printf("%f : ", veci[1]);
  // printf("%f", veci[2]);
  for(j = 0 ; j < ni ; j++) {
    dev = (veci[j] - vecj[j]);
    dist += dev * dev;
    // printf("%f",dev); // %d is integer
  }
  // if(count != ni) dist /= ((double)count/ni);
  return sqrt(dist);
}

double euclidean_d_c_dot(const double *veci, const double *vecj, int ni) {
  double dist;
  int j;
  double dev[ni]; // better to initialize it! we know its value

  // Is it the best for vector subtraction???
  for(j = 0 ; j < ni ; j++) {
    dev[j] = (veci[j] - vecj[j]);
  }

  // final dot mul
  dist = unroll_dot(ni, dev, dev);

  return sqrt(dist);
}

double euclidean_d_c_mag(const double *veci, const double *vecj, int ni) {
  double dev, dist;
  int j;
  // size_t nj = NELEMS(vecj); // would get longer!
  dist = 0;
  for(j = 0 ; j < ni ; j++) {
    dev = (veci[j] - vecj[j]);
    dist += dev * dev;
  }
  // if(count != ni) dist /= ((double)count/ni);
  return dist;
}

double maximum_d_c(const double *veci, const double *vecj, int ni)
{
  double dev, dist;
  int count, j;

  count = 0;
  dist = -DBL_MAX;
  for(j = 0 ; j < ni ; j++) {
    if(both_non_NA(veci[j], vecj[j])) {
      dev = fabs(veci[j] - vecj[j]);
      if(!ISNAN(dev)) {
          if(dev > dist) dist = dev;
          count++;
      }
    }
  }
  if(count == 0) return NA_REAL;
  return dist;
}
double manhattan_d_c(const double *veci, const double *vecj, int ni)
{
  double dev, dist;
  int count, j;

  count = 0;
  dist = 0;
  for(j = 0 ; j < ni ; j++) {
    if(both_non_NA(veci[j], vecj[j])) {
      dev = fabs(veci[j] - vecj[j]);
      if(!ISNAN(dev)) {
        dist += dev;
        count++;
      }
    }
  }
  if(count == 0) return NA_REAL;
  if(count != ni) dist /= ((double)count/ni);
  return dist;
}

double canberra_d_c(const double *veci, const double *vecj, int ni)
{
    double dev, dist, sum, diff;
    int count, j;

    count = 0;
    dist = 0;
    for(j = 0 ; j < ni ; j++) {
      if(both_non_NA(veci[j], vecj[j])) {
        sum = fabs(veci[j] + vecj[j]);
        diff = fabs(veci[j] - vecj[j]);
        if (sum > DBL_MIN || diff > DBL_MIN) {
          dev = diff/sum;
          if(!ISNAN(dev) || (!R_FINITE(diff) && diff == sum &&
          /* use Inf = lim x -> oo */ (int) (dev = 1.))) {
            dist += dev;
            count++;
          }
        }
      }
    }
    if(count == 0) return NA_REAL;
    if(count != ni) dist /= ((double)count/ni);
    return dist;
}

double binary_d_c(const double *veci, const double *vecj, int ni)
{
    int total, count, dist;
    int j;

    total = 0;
    count = 0;
    dist = 0;

    for(j = 0 ; j < ni ; j++) {
	if(both_non_NA(veci[j], vecj[j])) {
	    if(both_FINITE(veci[j], vecj[j])) {
		if(veci[j] != 0. || vecj[j] != 0.) {
		    count++;
		    if( ! (veci[j] != 0. && vecj[j] != 0.) ) dist++;
		}
		total++;
	    }
	}
    }

    if(total == 0) return NA_REAL;
    if(count == 0) return 0;
    return (double) dist / count;
}

double minkowski_d_c(const double *veci, const double *vecj, int ni, const double p)
{
    double dev, dist;
    int count, j;

    count= 0;
    dist = 0;
    // printf("%f", p);
    for(j = 0 ; j < ni ; j++) {
	     if(both_non_NA(veci[j], vecj[j])) {
	        dev = (veci[j] - vecj[j]);
	        if(!ISNAN(dev)) {
		          dist += pow(fabs(dev), p);
		          count++;
	        }
	     }
    }
    if(count == 0) return NA_REAL;
    if(count != ni) dist /= ((double)count/ni);
    return pow(dist, 1.0/p);
}
