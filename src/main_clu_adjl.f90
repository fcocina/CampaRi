!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 2.0                                                           !
!                                                                          !
!    Copyright (C) 2014, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger,               !
!                        Davide Garolini                                   !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN:      Andreas Vitalis                                               !
! WRAPPER:   Davide Garolini                                               !
!                                                                          !
!--------------------------------------------------------------------------!

! CHANGES IN VARIABLES
! dis_method = cdis_crit
! n_xyz = cdofsbnds !principally the length and starting point of the coordinate
! n_snaps = cstored
! trj_data(n_snaps,n_xyz) = cludata(n_xyz,n_snaps)
! tmp_d = rmsdtmp
! n_xyz = calcsz
! childr_alsz = chalsz
! I eliminated maxrads and the |clust(1|2)| = 1 distance for redundancy
!
! OLDERIES
! real(8), ALLOCATABLE :: maxrads(:) !vector of radius for each cluster
! This is a different variable from cluster radius because it is needed that
! cluster is complete to calculate it.
! I decided not to use the maxrads because it is not a useful calculation
! if the data-set is big enough to have cluster that has maxrads ~ radius
! dis_method = 'FMCSC_CDISTANCE'
! STUFF TO ELIMINATE
! cmaxrad, c_nhier, cprogindrmax, c_multires
!
subroutine generate_neighbour_list( &
  trj_data, n_xyz_in, n_snaps_in, dfffo, clu_radius_in, clu_hardcut_in, & !input
  adjl_deg, adjl_ix, adjl_dis, max_degr, & !output
  dis_method_in, metric_mat_in, & !algorithm details
  normalize_dis_in, return_tree_in_r, dump_tree_nc, mute_in) !modes

  use mod_gutenberg
  use mod_variables_gen
  use mod_clustering
  use mod_gen_nbls
#ifdef LINK_NETCDF
  use netcdf
#endif
  use mod_mst
  implicit none

  ! INPUT VARIABLES
  integer, intent(in) :: n_xyz_in !numbers of xyz (atoms*3)
  integer, intent(in) :: n_snaps_in !number of snapshots in input
  integer, intent(in) :: dfffo !dimensional_flag_for_fixed_out (netcdf workaround)
  integer, intent(in) :: dis_method_in !distance method
  real(8), intent(in) :: metric_mat_in(n_xyz_in, n_xyz_in) !distance mat input
  real(8), intent(in) :: trj_data(n_snaps_in, n_xyz_in) !trajectory input
  real(8), intent(in) :: clu_radius_in !defines the max cluster sizes
  real(8), intent(in) :: clu_hardcut_in !threshold between cluster snaps
  logical, intent(in) :: normalize_dis_in !flag for normalize the distance matrix
  logical, intent(in) :: return_tree_in_r !want to use R output?..
  logical, intent(in) :: dump_tree_nc !want to use netcdf dumps?..
  logical, intent(in) :: mute_in !verbose terminal output
  ! the mst option could generate an adjlist which was not an mst - only NO BIRCH

  ! HELPING AND DEBUGGING VARIABLES
  integer i ! helper variables
  integer nzeros ! nzeros = number of not connected components (dis .le. 0)
  ! character(len=1024) :: format_var !format for above mentioned dumping
  real(8) t2, t1 !timing variables
  ! logical run_online ! ----------------------------------------
  ! the last variable is momentaneously set to return_tree_in_r as it
  ! seems that a lot of files are not touched otherwise. (m_mst is not explored
  ! if the installation has already NETCDF in it)

  ! OUTPUT VARIABLES !strict output collaboration with R ! TODO: dummy var to lower memory need
  integer, intent(inout) :: max_degr !maximum degree of the adjlist
  integer, intent(inout) :: adjl_deg(dfffo)
  integer, intent(inout) :: adjl_ix(dfffo, dfffo)
  real(8), intent(inout) :: adjl_dis(dfffo, dfffo)

  ! N B :
  ! clu_hardcut is used for distances between different clusters snapshos as
  ! a threshold in leader clusting (MST).
  ! If the intent is in this cannot be an ALLOCATABLE variable

  n_xyz = n_xyz_in
  n_snaps = n_snaps_in
  mute = mute_in
  radius = clu_radius_in
  hardcut = clu_hardcut_in
  dis_method = dis_method_in
  normalize_dis = normalize_dis_in

  ! set the distance matrix if mode is 12
  if(dis_method .eq. 12) then
    allocate(distance_mat(n_xyz, n_xyz))
    distance_mat = metric_mat_in
  end if

  ! Internal defaults
  if (hardcut .lt. radius) hardcut = 2.0*radius
  firstcall = .true.

  !debugging flags
  rand_seed = 10 !it is used to have fixed seed
  !if it is 0 it uses the standard random_seed
  precise_clu_descr = 10 !dev var that shows the first 10 clusters


  call sl()
  if(dump_tree_nc) call spr("Netcdf memory handling active")
  if(return_tree_in_r) call spr("Selected return of the tree in R.")
  call spr('------------------------------------------------------------')
  call sipr("Selected distance: ", dis_method)
  call sipr("Number of snapshots: ", n_snaps)
  call sipr("Number of variables: ", n_xyz)
  call srpr("Radius for leader clustering: ", radius)
  call srpr("Hardcut for leader clustering: ", hardcut)
  call sl()

  ! now compare all blocks to each other (the slowest part) taking advantage
  ! of information generated previously (otherwise intractable)
  call CPU_time(t1)
  call spr('------------------------------------------------------------')
  call spr('Now computing cutoff-assisted neighbor list...')
  call leader_clustering(trj_data)
  call gen_nb(trj_data)
  call CPU_time(t2)
  call srpr('Time elapsed for neighbor list creation (s): ', t2-t1)
  call spr('Neighbor list generated.')
  call sl()

  ! check for possible wrongdoing in the neighbour list
  nzeros = 0
  do i=1,n_snaps
    if (cnblst(i)%nbs.le.0) then
      nzeros = nzeros + 1
      ! call sipr('Warning. Snapshot # ',i,' is with 0 degree'
   end if
  end do
  if (nzeros.gt.0) then
    call sipr( 'Warning. The following snapshots are without a neighbor &
    &(similar) structure. This may in some cases cause the clustering &
    &algorithm to misbehave.',nzeros)
  end if
  call sl()

  ! deallocate the clustering structure
  do i=1,n_clu_alc_sz_gen
    if (allocated(scluster(i)%snaps).EQV..true.) deallocate(scluster(i)%snaps)
    if (allocated(scluster(i)%sums).EQV..true.) deallocate(scluster(i)%sums)
    if (allocated(scluster(i)%tmpsnaps).EQV..true.) deallocate(scluster(i)%tmpsnaps)
    if (allocated(scluster(i)%children).EQV..true.) deallocate(scluster(i)%children)
  end do
  deallocate(scluster)

  ! creating the mst
  call CPU_time(t1)
  call gen_MST_from_nbl()
  call CPU_time(t2)
  call srpr( 'TIME elapsed for MST building (s): ',t2-t1)
  ! Dumping to netcdf if necessary
  ! this step  can be available ONLY if netcdf was used.
  ! the obj approxmst belongs only to m_mst_dumping
  if(return_tree_in_r) then
    call spr('Returning the tree in R...')
    call assign_tree(adjl_deg, adjl_ix, adjl_dis, max_degr)
    call spr('...done')
  end if
  if(dump_tree_nc) then
    call spr('DUMPING THE MST/SST to netcdf...')
    call CPU_time(t1)
    call dump_nbl_nc()
    call CPU_time(t2)
    call srpr('Time elapsed for mst dumping (s): ',t2-t1)
    call spr('...file-mst dumping done.')
    call spr('------------------------------------------------------------')
  end if

  ! ! if the obj does not exist this throw an error
  if (allocated(approxmst).EQV..true.) then
    do i=1,n_snaps
      if (allocated(approxmst(i)%adj).EQV..true.) deallocate(approxmst(i)%adj)
      if (allocated(approxmst(i)%dist).EQV..true.) deallocate(approxmst(i)%dist)
    end do
    deallocate(approxmst)
  end if
  !
  ! deallocate the distance matrix if mode is 12
  if(dis_method .eq. 12) deallocate(distance_mat)

  firstcall = .false.
end subroutine generate_neighbour_list
